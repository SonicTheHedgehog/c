#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <math.h>

const float pi = 3.141592;

struct Vec2Cart
{
    float x;
    float y;
};
struct Vec2Polar
{
    float radius;
    float angle;
};
struct line
{
    struct Vec2Cart point;
    float angle;
};
struct Color
{
    int red;
    int green;
    int blue;
};
struct circle
{
    struct Vec2Cart point;
    float rad;
};
struct movingCircle
{
    struct circle cir;
    struct Color color;
    int dir;
};
struct Everything
{
    float R;
    float r;
    float alpha;
    struct Vec2Polar A0;
    struct Vec2Cart A;
    struct line L;
    struct movingCircle S;
    struct Vec2Cart X;
    struct Vec2Cart Y;
};

struct Color randColor();
float lx(struct line line, float y);
struct Vec2Polar c2p(struct Vec2Cart vcart);
struct Vec2Cart p2c(struct Vec2Polar vpolar);
struct Vec2Cart vcadd(struct Vec2Cart v1, struct Vec2Cart v2);
int main()
{
    Console_clear();
    Canvas_invertYOrientation();
    int w = 50, h = 50;
    Canvas_setSize(w, h);
    srand(time(0));
    const int N = rand() % (6 - 1 + 1) + 1;
    struct Everything every[N];
    int FSP = 20;
    int delay = 1000 / FSP;
    const float dt = delay / 1000.0;
    struct Vec2Cart C = {w / 2, h / 2};
    for (int i = 0; i < N; i++)
    {
        every[i].R = (rand() / (float)RAND_MAX) * (25 - (-25)) + (-25);
        every[i].r = (rand() / (float)RAND_MAX) * (10 - 1) + 1;
        every[i].alpha = (rand() / (float)RAND_MAX) * ((2 * pi) - 0) + 0;
        every[i].A0.radius = every[i].R;
        every[i].A0.angle = every[i].alpha;
        every[i].L.point = C;
        every[i].L.angle = every[i].alpha;
        every[i].S.color = randColor();
        every[i].S.dir = 0;
    }
    while (!Console_isKeyDown())
    {
        for (int i = 0; i < N; i++)
        {
            if ((every[i].A.x * every[i].A.y < (h - 1) * (w - 1)) && (every[i].A.x < w - 1) && (every[i].A.y < w - 1) && (every[i].A.y > 0) && (every[i].A.x > 0))
            {
                if (every[i].S.dir == 0)
                {
                    every[i].A0.radius += 1;
                }
                if (every[i].S.dir == 1)
                {
                    every[i].A0.radius -= 1;
                }
            }
            else
            {
                if (every[i].S.dir == 1)
                {
                    every[i].S.dir = 0;
                    every[i].A0.radius += 1;
                }
                else if (every[i].S.dir == 0)
                {
                    every[i].S.dir = 1;
                    every[i].A0.radius -= 1;
                }
            }
        }
        for (int i = 0; i < N; i++)
        {
            every[i].A = vcadd(C, p2c(every[i].A0));
        }
        for (int i = 0; i < N; i++)
        {
            every[i].S.cir.point = every[i].A;
            every[i].S.cir.rad = every[i].r;
        }
        for (int i = 0; i < N; i++)
        {
            every[i].X.x = lx(every[i].L, 0);
            every[i].X.y = 0;
            every[i].Y.x = lx(every[i].L, 49);
            every[i].Y.y = 49;
        }
        Canvas_beginDraw();
        Canvas_setColorRGB(255, 255, 255);
        Canvas_fillRect(0, 0, 50, 50);
        Canvas_setColorRGB(153, 221, 255);
        for (int i = 0; i < N; i++)
        {
            Canvas_strokeLine(every[i].X.x, every[i].X.y, every[i].Y.x, every[i].Y.y);
        }
        Canvas_setColorRGB(255, 0, 43);
        Canvas_putPixel(C.x, C.y);
        for (int i = 0; i < N; i++)
        {
            Canvas_setColorRGB(every[i].S.color.red, every[i].S.color.green, every[i].S.color.blue);
            Canvas_fillCircle(every[i].S.cir.point.x, every[i].S.cir.point.y, every[i].S.cir.rad);
        }
        Canvas_setColorRGB(255, 255, 128);
        for (int i = 0; i < N; i++)
        {
            Canvas_putPixel(every[i].A.x, every[i].A.y);
        }

        Canvas_endDraw();

        for (int i = 1; i < N; i += 2)
        {
            every[i].L.angle += pi / 3 * dt;
            every[i].A0.angle += pi / 3 * dt;
        }
        for (int i = 0; i < N; i += 2)
        {
            every[i].L.angle -= pi / 3 * dt;
            every[i].A0.angle -= pi / 3 * dt;
        }
        sleepMillis(delay);
    }
}
float lx(struct line line, float y)
{
    float k = tan(line.angle);
    float b = line.point.y - k * line.point.x;
    return (y - b) / k;
}
struct Vec2Cart p2c(struct Vec2Polar vpolar)
{
    struct Vec2Cart vcart = {
        vpolar.radius * cos(vpolar.angle),
        vpolar.radius * sin(vpolar.angle),
    };
    return vcart;
}
struct Vec2Polar c2p(struct Vec2Cart vcart)
{
    struct Vec2Polar vpolar = {
        sqrt(pow(vcart.x, 2) + pow(vcart.y, 2)),
        atan2(vcart.y, vcart.x),
    };
    return vpolar;
}
struct Vec2Cart vcadd(struct Vec2Cart v1, struct Vec2Cart v2)
{
    struct Vec2Cart vres = {
        v1.x + v2.x,
        v1.y + v2.y};
    return vres;
}
struct Color randColor()
{
    struct Color c;
    c.red = rand() % (255 - 0 + 1);
    c.green = rand() % (255 - 0 + 1);
    c.blue = rand() % (255 - 0 + 1);
    return c;
}