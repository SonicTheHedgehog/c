#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <math.h>

struct Vec2D
{
    float x;
    float y;
};
struct Vec2D vad(struct Vec2D c, struct Vec2D b);
struct line
{
    struct Vec2D point;
    float alpha;
};
float lx(struct line line, float y)
{
    float k = tan(line.alpha);
    float b = line.point.y - k * line.point.x;
    return (y - b) / k;
}
int main()
{
    Canvas_setSize(50, 50);
    Console_clear();
    Canvas_invertYOrientation();
    float x = 25, y = 25, R1 = 15, R2 = 5, pi = 3.141592, a = 225;
    while (1)
    {
        float alpha = (a * pi) / 180;
        struct Vec2D C = {x, y};
        struct Vec2D B =
            {
                R1 * cos(alpha),
                R1 * sin(alpha),
            };
        struct Vec2D A = vad(C, B);
        struct line L = {C, alpha};
        struct Vec2D X = {lx(L, 0), 0};
        struct Vec2D Y = {lx(L, 49), 49};
        Canvas_beginDraw();
        Canvas_setColorRGB(0, 0, 0);
        Canvas_fillRect(0, 0, 50, 50);
        Canvas_setColorRGB(240, 230, 140);
        Canvas_strokeLine(X.x, X.y, Y.x, Y.y);
        Canvas_setColorRGB(255, 0, 0);
        Canvas_putPixel(C.x, C.y);
        Canvas_setColorRGB(100, 149, 237);
        Canvas_fillCircle(A.x, A.y, R2);
        Canvas_setColorRGB(255, 225, 0);
        Canvas_putPixel(A.x, A.y);
        Canvas_endDraw();
        printf("Choose komand:");
        char p = Console_getChar();
        if (p == 'w')
            a += pi / 4;
        else if (p == 's')
            a -= pi / 4;
        else if (p == 'x')
        {
            x++;
            if (x >= 50)
            {
                x--;
            }
        }
        else if (p == 'z')
        {
            x--;
            if (x <= 0)
            {
                x++;
            }
        }
        else if (p == 'v')
        {
            y++;
            if (y >= 50)
            {
                y--;
            }
        }
        else if (p == 'c')
        {
            y--;
            if (y <= 0)
            {
                y++;
            }
        }
        else if (p == 'e')
        {
            R1++;
        }
        else if (p == 'q')
        {
            R1--;
        }
        else if (p == 'd')
        {
            R2++;
        }
        else if (p == 'a')
        {
            R2--;
            while (R2 <= 0)
            {
                R2 = 1;
            }
        }
        else
            break;
    }
    return 0;
}
struct Vec2D vad(struct Vec2D c, struct Vec2D b)
{
    struct Vec2D a;
    a.x = c.x + b.x;
    a.y = c.y + b.y;
    return a;
}