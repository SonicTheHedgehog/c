#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int number_main(FILE *fin, FILE *fout);
int text_main(FILE *fin, FILE *fout);
int main(int argc, char *argv[argc])
{
    if (argc < 3)
    {
        fprintf(stderr, "ERROR\n");
        return EXIT_FAILURE;
    }
    const char *inFileName = argv[1];
    FILE *fin = fopen(inFileName, "r");
    if (fin == NULL)
    {
        fprintf(stderr, "Error opening file '%s'\n", inFileName);
        return EXIT_FAILURE;
    }

    char taskType[10];
    char *res = fgets(taskType, 10, fin);
    if (res == NULL)
    {
        fprintf(stderr, "File is empty '%s'\n", inFileName);
        fclose(fin);
        return EXIT_FAILURE;
    }
    if (strncmp(res, "numbers", 7) != 0 && strncmp(res, "text", 4) != 0)
    {
        fprintf(stderr, "%s: error\n", inFileName);
        fclose(fin);
        return EXIT_FAILURE;
    }
    else
    {
        const char *outFileName = argv[2];
        FILE *fout = fopen(outFileName, "w");
        if (fout == NULL)
        {
            fprintf(stderr, "Error opening file '%s'\n", outFileName);
            fclose(fin);
            return EXIT_FAILURE;
        }
        if (strncmp(res, "numbers", 7) == 0)
        {
            number_main(fin, fout);
        }
        else if (strncmp(res, "text", 4) == 0)
        {
            text_main(fin, fout);
        }
        fclose(fout);
    }
    fclose(fin);
    return 0;
}
struct DynArray
{
    int *array;
    int capacity;
    int length;
};
void dynarray_init(struct DynArray *arr)
{
    const size_t initial_capacity = 16;
    arr->capacity = initial_capacity;
    arr->array = malloc(initial_capacity * sizeof(int));
    arr->length = 0;
}
void dynarray_deinit(struct DynArray *arr)
{
    free(arr->array);
    arr->length = 0;
    arr->capacity = 0;
}
void dynarray_print(struct DynArray *arr)
{
    for (int i = 0; i < arr->length; i++)
    {
        printf("%i ", arr->array[i]);
    }
    puts("");
}
void dynarray_realloc(struct DynArray *pdarr)
{
    pdarr->capacity *= 2;
    int *new_array = realloc(pdarr->array, pdarr->capacity * sizeof(int));
    if (new_array == NULL)
    {
        free(pdarr->array);
        fprintf(stderr, "Memory reallocation error for %s", __func__);
        exit(1);
    }
    else
    {
        pdarr->array = new_array;
    }
}
void dynarray_push_back(struct DynArray *pdarr, int new_val)
{
    if (pdarr->length == pdarr->capacity)
        dynarray_realloc(pdarr);
    pdarr->array[pdarr->length] = new_val;
    pdarr->length += 1;
}

int number_main(FILE *fin, FILE *fout)
{
    int k = 0;
    int amount = 0;
    struct DynArray myarray;
    dynarray_init(&myarray);
    while (fscanf(fin, "%i", &myarray.array[k]) > 0)
    {
        dynarray_push_back(&myarray, myarray.array[k]);
        k++;
    }
    for (int i = 0; i < myarray.length; i++)
    {
        if (myarray.array[i] > 0)
        {
            for (int j = 2; j <= myarray.array[i]; j++)
            {
                if (myarray.array[i] % j == 0)
                {
                    amount++;
                }
            }
            if (amount == 1)
            {
                fprintf(fout, "%i ", myarray.array[i]);
            }
            amount = 0;
        }
    }
    dynarray_print(&myarray);
    dynarray_deinit(&myarray);
    return 0;
}

struct StrDynArray
{
    char **array;
    size_t capacity;
    size_t length;
};
char *dynstring(const char *str)
{
    char *p = malloc((strlen(str) + 1) * sizeof(char));
    strcpy(p, str);
    return p;
}
char *readTextLine(FILE *fin)
{
    if (feof(fin) != 0)
    {
        return NULL;
    }
    int n = 10;
    char buff[n];
    char *new_str = malloc(sizeof(char) * n);
    while (1)
    {
        char *tmp = fgets(buff, n, fin);
        if (tmp == NULL)
            break;
        strcat(new_str, buff);
        if (strchr(buff, '\n') != NULL)
        {
            break;
        }
        char *p = realloc(new_str, sizeof(p) * (n + 10));
        if (p == NULL)
        {
            break;
        }
        new_str = p;
    }
    return new_str;
}
void strdynarray_init(struct StrDynArray *p)
{
    p->capacity = 4;
    p->array = malloc(p->capacity * sizeof(char *));
    p->length = 0;
}
void strdynarray_deinit(struct StrDynArray *p)
{
    free(p->array);
    p->capacity = 0;
    p->length = 0;
}
void strdynarray_realloc(struct StrDynArray *p)
{
    p->capacity *= 2;
    char **tmp = realloc(p->array, p->capacity * sizeof(char *));
    if (tmp == NULL)
    {
        free(p->array);
        fprintf(stderr, "Reallocation error\n");
        exit(1);
    }
    else
    {
        p->array = tmp;
    }
}
void strdynarray_push_back(struct StrDynArray *p, char *str)
{
    if (p->length == p->capacity)
        strdynarray_realloc(p);
    p->array[p->length] = str;
    p->length += 1;
}

int text_main(FILE *fin, FILE *fout)
{
    struct StrDynArray lines = {0};
    char letters[12] = "AEIOUYaeiouy";
    strdynarray_init(&lines);
    while (1)
    {
        char *pline = readTextLine(fin);
        if (pline == NULL)
            break;
        strdynarray_push_back(&lines, pline);
    }
    int k = lines.length;
    for (int i = 0; i + 1 < k; i++)
    {
        printf("> %s", lines.array[i]);
        for (int j = 0; letters[j] != '\0'; j++)
        {
            if (strncmp(lines.array[i], &letters[j], 1) == 0)
            {
                fprintf(fout, "%s ", lines.array[i]);
            }
        }
    }
    for (int i = 0; i < k; i++)
    {
        free(lines.array[i]);
    }
    strdynarray_deinit(&lines);
    return 0;
}