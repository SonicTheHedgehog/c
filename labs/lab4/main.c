#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
float fy(float x)
{
    float y = 0;
    if ((x > -5) && (x <= 3))
    {
        y = pow((x + 3), 3) + (1 / x);
    }
    else
    {
        y = 0.3 * tan(x) + 2;
    }
    return y;
}

int array()
{
    srand(time(0));
    int arrA[30];
    int arrB[20] = {0};
    int i = 0;
    int c = 0;
    printf("A={ ");
    for (i = 0; i < 30; i++)
    {
        arrA[i] = rand() % (19 - 0 + 1) + (0);
        printf("%i ", arrA[i]);
    }
    int arrC[35];
    printf("}\n");
    for (int n = 0; n < 20; n++)
    {
        for (int i = 0; i < 30; i++)
        {
            if (arrA[i] == n)
            {
                c++;
            }
        }
        printf("B[%i]=%i;\n", n, c);
        c = 0;
        i = 0;
    }
    printf("\n");
}
float matrix(float x_min, float x_max)
{
    float arr[2][20];
    float x_step = (x_max - x_min) / 20;
    if (x_max < x_min)
    {
        printf("Wrong data. Try again!");
    }
    else
    {
        for (int j = 0; j < 20; j++)
        {
            arr[0][j] = x_min + x_step * j;
            if (arr[0][j] == 0)
            {
                printf("Error");
            }
            else
            {
                arr[1][j] = fy(arr[0][j]);
            }
        }
        printf("A={ ");
        for (int j = 0; j < 20; j++)
        {
            printf("%.2f ", arr[0][j]);
        }
        printf("} \n  { ");
        for (int j = 0; j < 20; j++)
        {
            printf("%.2f ", arr[1][j]);
        }
        printf("} \n\n ");
    }
}
int main()
{
    float x_min = 0;
    float x_max = 0;
    int h = 0;
    printf("Choose:\n 1. Int array\n 2. Float matrix\n 3. Quit\n ");
    scanf("%i", &h);
    while (h != 3)
    {
        if (h == 1)
        {
            array();
        }
        else if (h == 2)
        {
            printf("Enter min and max: ");
            scanf("%f %f", &x_min, &x_max);
            matrix(x_min, x_max);
        }
        else if (h == 3)
        {
            return 0;
        }
        else 
        {
            printf("Incorect! \n");
        }
        printf("Choose:\n 1. Int array\n 2. Float matrix\n 3. Quit\n ");
        scanf("%i", &h);
    }
}