#include <stdio.h>
#include <math.h>


int main(void)
{
    const float xmin  = -10.0;
    const float xmax  = 10.0;
    const float xstep = 0.5;
    float x=xmin;
    float y=0;
    while (x<=xmax) 
    {
        if ((x > -5) && (x <= 3))
        { 
            if (x==0) 
            {
                printf("y(%.1f) = Error\n",x);
            }
            else
            {
                y=pow((x+3),3)+(1/x);
                printf("y(%.1f) = %.3f\n",x,y);  
            }
        }
        else 
        {
            y=0.3*tan(x)+2;
            printf("y(%.1f) = %.3f\n",x,y);
        }
    x += xstep;
    }
    return 0;
}