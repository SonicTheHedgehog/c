#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
double fy(double x)
{
    double y = 0;
    if ((x > -5) && (x <= 3))
    {
        y = pow((x + 3), 3) + (1 / x);
    }
    else
    {
        y = 0.3 * tan(x) + 2;
    }
    return y;
}

double int_fy(double x_min, double x_max, double x_step)
{
    double x = x_min + x_step;
    double c = 0;
    for (c; x_max > x; x += x_step)
    {
        c += fy(x);
    }
    double res = x_step * (fy(x_max) + c);
    return res;
}

double main()
{
    double x_min, x_max, x_step;
    double pi = 3.1415;
    double a = pi / 2.;
    printf("Enter min and max of the integral:\n");
    scanf("%lf%lf", &x_min, &x_max);
    printf("Enter step size:\n");
    scanf("%lf", &x_step);
    if (x_min >= x_max)
    {
        printf("Incorrect data. Try again\n");
    }
    else if (((x_min < 0) && (x_max > 0))|| (x_min == 0) || (x_max == 0))
    {
        printf("Error\n");
    }
    else if ((x_max >= 3) && (x_min >= 3) || (x_max < -5) && (x_min < -5))
    {
        while (a < x_min)
        {
            a -= pi;
        }
        while (a > x_max)
        {
            a += pi;
        }
        if (x_min <= a && a <= x_max)
        {
            printf("Error\n");
        }
    }
    else
    {
        printf("Result = %lf\n", int_fy(x_min, x_max, x_step));
    }
    return 0;
}