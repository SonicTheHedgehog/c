#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#define red "\x1B[31m"
#define green "\x1b[32m"
#define black "\x1B[0m"
struct DynArray
{
    int *array;
    int capacity;
    int length;
};
struct SLNode
{
    float data;
    struct SLNode *next;
};
void dynarray_print(struct DynArray *arr, int from, int to)
{
    for (int i = from; i < to; i++)
    {
        printf("%i ", arr->array[i]);
    }
    printf("\nCapacity: %i\nLength: %i\n", arr->capacity, arr->length);
}
void dynarray_realloc(struct DynArray *arr, int len)
{
    arr->capacity = len * 2;
    int *new_array = realloc(arr->array, arr->capacity * sizeof(int));
    if (new_array == NULL)
    {
        free(new_array);
        printf("Переповнення доступної процесу пам’яті у кучі");
    }
    else
    {
        arr->array = new_array;
    }
}
void dynarray_init(struct DynArray *arr)
{
    const size_t init_capacity = 3;
    arr->array = malloc(init_capacity * sizeof(int));
    if (arr->array == NULL)
    {
        arr->length=10;
        dynarray_realloc(arr, arr->length);
        exit(1);
    }
    arr->capacity = init_capacity;
    arr->length = 0;
    printf(red "Init: \n" black);
    dynarray_print(arr, 0, arr->length);
}
void dynarray_deinit(struct DynArray *arr)
{
    free(arr->array);
    arr->length = 0;
    arr->capacity = 0;
    printf(red "Deinit.\n" black);
}
void dynarray_clear(struct DynArray *arr)
{
    printf(red "Clear:\n" black);
    for (int i = 0; arr->length != 0; i++)
    {
        arr->array[i] = 0;
        arr->length--;
    }
    dynarray_print(arr, 0, arr->length);
}
void dynarray_resize(struct DynArray *arr, int len, int value)
{
    printf(red "Resize: \n" black);
    printf("New length= %i\nValue= %i\n", len, value);
    if (len > arr->length && len <= arr->capacity)
    {
        for (int i = arr->length; i < len; i++)
        {
            arr->array[i] = value;
        }
        arr->length = len;
        dynarray_print(arr, 0, arr->length);
    }
    else if (len > arr->capacity)
    {
        arr->length = len;
        dynarray_realloc(arr, arr->length);
        for (int i = 0; i < arr->length; i++)
        {
            arr->array[i] = value;
        }
        dynarray_print(arr, 0, arr->length);
    }
}
void dynarray_insert(struct DynArray *arr, int index, int value)
{
    printf(red "Insert: \n" black);
    printf("Index= %i\nValue= %i\n", index, value);
    arr->length += 1;
    if (arr->length >= arr->capacity)
    {
        dynarray_realloc(arr, arr->length);
        for (int i = arr->length; i >= index; i--)
        {
            arr->array[i] = arr->array[i - 1];
        }
        arr->array[index - 1] = value;
        dynarray_print(arr, 0, arr->length);
    }
    else
    {
        for (int i = arr->length; i >= index; i--)
        {
            arr->array[i] = arr->array[i - 1];
        }
        arr->array[index - 1] = value;
        dynarray_print(arr, 0, arr->length);
    }
}
void dynarray_pop_front(struct DynArray *arr)
{
    printf(red "Pop_front:\n" black);
    for (int i = 0; i >= arr->length; i++)
    {
        arr->array[i] = arr->array[i + 1];
    }
    arr->length -= 1;
    dynarray_print(arr, 0, arr->length);
}
void dynarray_main()
{
    struct DynArray arr1;
    dynarray_init(&arr1);
    dynarray_resize(&arr1, 10, 5);
    dynarray_insert(&arr1, 9, 8);
    dynarray_pop_front(&arr1);
    dynarray_clear(&arr1);
    dynarray_deinit(&arr1);
}
struct SLNode *slnode_create(float data)
{
    struct SLNode *pnode = malloc(sizeof(struct SLNode));
    pnode->data = data;
    pnode->next = NULL;
    return pnode;
}
void slnode_print(struct SLNode *node)
{
    if (node == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        while (node != NULL)
        {
            printf("%.3f->", node->data);
            node = node->next;
        }
        puts(" ");
    }
}
struct SLNode *slnode_back(struct SLNode *head)
{
    if (head == NULL)
        return NULL;
    struct SLNode *node = head;
    while (node->next != NULL)
    {
        node = node->next;
    }
    return node;
}
struct SLNode *slnode_push_front(struct SLNode *head, float value)
{
    printf(red "Push front: " black "%.3f\n", value);
    struct SLNode *new_node = slnode_create(value);
    new_node->next = head;
    return new_node;
}
struct SLNode *slnode_push_back(struct SLNode *head, float value)
{
    printf(red "Push back: " black "%.3f\n", value);
    struct SLNode *new_node = slnode_create(value);
    if (head == NULL)
    {
        return new_node;
    }
    else
    {
        struct SLNode *back = slnode_back(head);
        back->next = new_node;
        return back;
    }
}
void slnode_size(struct SLNode *node)
{
    int size = 0;
    while (node != NULL)
    {
        node = node->next;
        size++;
    }
    printf(red "Size" black "= %i", size);
}
void slnode_count_pos(struct SLNode *node)
{
    int pos = 0;
    while (node != NULL)
    {
        if (node->data > 0)
        {
            pos++;
        }
        node = node->next;
    }
    printf(red "\nPositive" black "= %i", pos);
}
void slnode_sum(struct SLNode *node)
{
    float sum = 0.0;
    if (node == NULL)
    {
        printf(red "\nSummary" black "= NAN");
    }
    else
    {
        while (node != NULL)
        {
            sum = sum + node->data;
            node = node->next;
        }
        printf(red "\nSummary" black "= %.3f", sum);
    }
}
void slnode_clear(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        struct SLNode *next = node->next;
        free(node);
        node = next;
    }
    printf(red "\nClear." black);
}
void slnode_main()
{
    struct SLNode *head = NULL;
    slnode_print(head);
    slnode_clear(head);
    head = slnode_push_front(head, 8.34);
    slnode_print(head);
    head = slnode_push_back(head, -65.47);
    head = slnode_push_front(head, -13.92);
    slnode_print(head);
    slnode_push_back(head, 23.75);
    slnode_print(head);
    slnode_size(head);
    slnode_count_pos(head);
    slnode_sum(head);
    slnode_clear(head);
}
int main()
{
    printf(green "Dynnarray:\n" black);
    dynarray_main();
    printf(green "\nSlnode:\n" black);
    slnode_main();
    puts(" ");
    return 0;
}